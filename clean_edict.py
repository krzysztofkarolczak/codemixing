#!/usr/local/bin/python
# encoding: utf-8

# author: Krzysztof Karolczak
# date: 09.08.2013

"""
Produces a parsed version of edict dictionary in the format:
[kanji];[reading] | [first_definition from edict] | [difficulty]

The difficulty can be in range <-1,16> where numbers 0-16 correspond to groups 
of 500 words in word occurrence frequency list (ie. if a word is 1-500 on the freq list 
it is given the difficulty of 0, 501-1000 = 1, ...) and -1 is assigned to words not
found in the dictionary.
"""

import re
import logging
import codecs

EDICT_PATH = "res/edict.utf" 
EDICT_CLEANED_PATH = "res/edict_cleaned.utf"
EDICT_BYDIFF_PATH = "res/diff/edict_diff_"
FREQ_LIST_PATH = "res/freq_list.csv"

def difficulty_estimation(freq_dict, words):
    """
    Assigns difficulty to a word based on frequency of occurrence list.
    Values range from <-1,8> where 0 is easy, 8 is hard and -1 corresponds to 'not found'.
    In case of multiple words the phrase is assigned with a difficulty of the highest rated word.
    """
    max_diff = -1;
    words = words.split(" ")
    for word in words:
        if word in freq_dict and len(word) > 0:
            diff = int(freq_dict[word])
            diff2 = int(diff/500)
            
            if(diff2 > max_diff):
                max_diff = diff2
                        
    return max_diff

#logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.ERROR)
logger=logging.getLogger(__name__)

## Open file resources

edict = []
freq_dict = {}
dict_diff = []

dict_old = codecs.open(EDICT_PATH, "r+", "utf-8")
dict_new = codecs.open(EDICT_CLEANED_PATH, "w", "utf-8")

for x in range(0, 16):
    dict_diff.append(codecs.open(EDICT_BYDIFF_PATH + str(x) + ".utf", "w", "utf-8"))

freq_list = codecs.open(FREQ_LIST_PATH, "r+", "utf-8")

## Read in the edict dictionary

for s in dict_old:
    edict.append(s.rstrip())

## Read in the frequency lit

for p in freq_list:
    sp = p.split(";")
    freq_dict[sp[0]] = sp[1]
    
## Using a regex remove suspicious edict lines and split the lines into groups

for line in edict:
    m = re.match("^(.*)(\s+)(\[)(.*)(\])(.+)", line)
    
    if m:
        translation = m.group(6).split("/")[1]
        translation = re.sub("\(.*?\)", "", translation).strip()
        
        ## Write the line in the new format to output file
        diff = difficulty_estimation(freq_dict, translation);
        dict_new.write(m.group(1) + ";" + m.group(4) + " | " + translation + " | " + str(diff) + "\n")
        
        if(diff > 0 and translation.count(' ') == 0):
            dict_diff[diff].write(translation + "\n")
        
dict_new.close()

for x in range(0, 16):
    dict_diff[x].close()