My Student Life

1.0 Introduction

Currently, as one of the exchange students, I have been studying at the International
Education Center at Waseda University in Japan. First, before coming to Japan, my most
important goal was to to maximize the power of my Japanese to some higher level. I wanted
to achieve an operational capacity of Japanese to find work after coming back to my
country or find a job later in Japan. I pursued that career because my major in college in
Korea was Japanese.

Apart from Japanese language I am also interested in the other aspects of Japan, such as
Japanese culture, history and society of Japan. It was really hard to be understood by
others and to extend the conversational ability of Japanese. When I retrospect the past, I
was put in the situation where I could not be understood but I was trying to put more
attention to improve my abilities and getting to know the fundamental things about this
country.

By the way, only a few days after I came to Japan I was very lucky. I was able to find a
part-time job one step ahead of other people at the introduction organized by my seniors.
That is when my life in Japan truly began. Because it was the first time for me in a
foreign country, there was many unfamiliar things. I was living alone and was hard to
start new relationships with people. And for me, at first hanging out with foreigners who
came from various countries was a big problem.

After a while, doing part time jobs and taking classes occupied my time. My life became
just a simple repetition of going to do my job and coming back home immediately after
this. I felt like I was missing something important, so I decided to reconsider things one
more time.

2.0 My experience

2.1 Preparation and basic knowledge of Japan

From the time I was studying in high school, I had a special interest in language. I began
to think that once I get in college, I would try to study linguistics. I was studying
French and German at the time. I chose German as a second language merely for the purpose
of college entrance examination and I was not particularly interested in deeply studying
it. When I get into the university I felt ready to study the Japanese language. I chose
this language because it is related to our country (Korea) and in fact we are neighboring
countries. Therefore, I decided to major in Japanese at the university. Since that time, I
started to learn grammar and Japanese expressions, read Japanese novels, history, etc..

Japanese itself, was interesting, but when it comes to study history and novels,
understanding the society of Japan was more difficult than I thought. Little by little,
for some reason I started to get tired. Just like in high school I had to recklessly study
for approaching tests and remember many things. Some people say that the language of the
country is reflected in many aspects of its culture and by its society. It was very
difficult for me to study the language while I did not know the overall situation in the
country and I came to understand that studying different things simmultaneously should be
done frome the beginning.

Then, around the time junior year was over, I got blessed with a really good opportunity
to study as an exchange student at Waseda University in Japan. Rather than just studying
and doing homework, I have been able to experience the student life in many ways. I also
realized that life in foreign country is different. When I talked to students from foreign
countries I noticed that even compared to them I did not know much about Japan.

2.2 The differences between cultures that I felt

“The difficulties of working hard everyday, beatiful towns and nature, awareness and
kindness of Japanese people”: this was a very simple image of Japan before I came to live
in this country.  I was finally able to contact with the ordinary Japanes society. It did
not happen at school, but from the time I started a part-time job. I was cleaning various
office large building in Shinjuku. When I was there, I could perceive the one side of
Japanese people.

“Japanese are worker bees” is a common belief about this nation. I could sense it from
people working in the office. They were working hard and staying late hours just like they
forgot that it is the time to go back home. I was finishing my part time job around 8 in
the evening and when I was going back home I could see a lot of people coming back home
suddently. I was thinking, “These people really like to work! I wonder if it is fun for
them”. While I was looking at them passing close to me I also thought “This is one of the
reason why Japan beame the economic superpower”. The other thing I felt in my day-to-day
life was the politeness and kindness of the Japanese. For example, they often use "I'm
sorry", and "if you like",  "please", etc.

I heard such a word on a daily basis. They are trying to not allow any trouble to other
people and apologize in the situations where no apology is necessary. It is because of
their consideration to others. Some people go with their kindness a little too far and
sometimes it felt like a burden to me, but I think it is a good culture. If you can use
the same language as described above, you may have been accustomed to a country called
Japan a little.

2.3 Student life

International students are spending every day actively. Some take part in club activities
after school or head to the library or just sit down on a bench.  Some have more free
time, the others in order to earn a living must haste and don’t have time for conversation
with friends.

When I Came to Japan at the end of March, the biggest goal was to improve my ability to
speak Japanese. I was attending classes and also studied hard by myself, trying to extend
my ability frequently. Then, when I came back to my country I wanted to be in the place
where I could use this knowledge.  I also find out that you cannot just simply learn the
language to improve your abiity. Above all, if you don’t understand the overall situation
of Japan and its culture, it is really hard to study just the language. I was given a
chance to stay there for one year and experience lots of things. Experiencing as much as
possible is the thing that should also be pursued from the student life abroad.


3.0 Final remarks

After all, studying and living in Japan was an unforgetable experience. To begin a new
life in Japan away from the home country where I was  used to life, was a great way to
challenge myself. Study abroad experience in Japan is also useful in work and life after
returning home, especially for me whose major is Japanese. In the Future, I really want to
match well my major and life. Therefore, I really recommend such experience to other
people, especially those who learn Japanese. I think for me this was a  chance  of
cross-cultural experience itself, to get some understanding of Japanese culture, and I
could experience a variety of Japan and in my  duration of your stay I fully used this
opportunity!
