#!/usr/local/bin/python
# encoding: utf-8

# author: Krzysztof Karolczak
# date: 09.08.2013

"""
Prepares the Japanese input text by finding all the nouns and looking for appropriate English translations.
The output text has the selected nouns preceded by an index and the file is supplemented with a list of all nouns
with the proposed translations. 

The goal of this step is allowing the teacher to proof read the text and eliminate
all possible mistakes before handing the materials over to the students.
"""

import os
import logging
import codecs

IN_FILE_PATH = "res/texts_input/2.txt"
OUT_FILE_PATH = "res/texts_output/2.txt"
EDICT_CLEANED_PATH = "res/edict_cleaned.utf"
JUMAN_RESULTS_PATH = "tmp/juman_results.txt"

#logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.ERROR)
logger=logging.getLogger(__name__)

## Read the dictionary, extract translations

edict = {}
diff = {}

dict_file = codecs.open(EDICT_CLEANED_PATH, "r+", "utf-8")

#Format of the dictionary: [kanji];[reading] | [first_definition from edict] | [difficulty]

for s in dict_file:
    s_split = s.split("|")
    # Create a kanji;reading -> translation dictionary
    edict[s_split[0].strip()] = s_split[1].strip()
    # Create a kanji;reading -> difficulty
    diff[s_split[0].strip()] = s_split[2].strip()

## Open source text

with open(IN_FILE_PATH, 'r') as content_file:
    content = content_file.read()

## Run through juman
    
command =  'juman -b < ' + IN_FILE_PATH + ' > ' + JUMAN_RESULTS_PATH
logger.debug(command)
os.system(command.encode("utf-8"))
jumanfile = codecs.open(JUMAN_RESULTS_PATH, 'r', "utf-8")
    
## Extract the nouns from juman output
    
nouns = []

for jum in jumanfile:
    jum = jum.rstrip()
    
    jumline = jum.split()
    if (not jumline or jumline[0] == '@' or jumline[0] == 'EOS'):
        continue

    word = jumline[2]
    pos = jumline[3]
    ## We also extract the reading as help for finding the correct English equivalent
    reading = jumline[1]
    
    word_reading = word + ";" + reading

    ## We are only interested in words that:
    ##  a) are nouns
    ##  b) haven't been added before
    ##  c) we have a translation for them
    ##  d) are on our frequency list and are not in the first 500 most common words
    ##  (e) and the translation consists of no more than 3 words)
    
    if (pos == u'名詞'):
        logger.debug(word_reading) 
        if(word_reading not in nouns) and (word_reading in edict) and (int(diff[word_reading]) > 0): #and (edict[word_reading].count(" ") < 3):
            nouns.append(word_reading)
            
## For each noun - find a translation
i = 1
translations = {}

## Due to a problem with replacing kanji multiple times (if it is used to compose a different word)
## the workaround is to sort words by length, replace them (in that order) with numbers ie. (1). (2) 
## and finally replace the numbers with number + word.   
 
nouns_sorted = sorted(nouns, key=len, reverse=True);

for noun in nouns_sorted:
    word = noun.split(";")
    
    logger.debug("%s", noun)

    i = nouns.index(noun) + 1
    num = "(" + str(i) + ")"
    
    ## Do the replacement to number
    content = content.replace(word[0], num)
    ## Prepare the translation line o be added
    translations[i] = num + word[0] + " | " + edict[noun] + " | " + diff[noun] + "\n"
    
j = 1
for noun in nouns:
    num = "(" + str(j) + ")"
    word = noun.split(";")
    ## Do the 2nd replacement - number to a number + word
    content = content.replace(num, num + word[0])
    j += 1

## We also calculate the average difficulty of nouns in the text        
sum_diff = 0
        
with open(OUT_FILE_PATH, "w") as outs:
    ## Output the text with words and their indexes
    outs.write(content + "\n\n ------------------------------------------- \n\n")
    ## Output the translations sorted by keys (so we preserve the order of appearance)
    for key in sorted(translations.iterkeys()):
        outs.write(translations[key])
        sum_diff += int(translations[key].split("|")[2])
        
    logger.debug("Sum: %d | No %d", sum_diff, len(translations))
    
    mean = (sum_diff + 0.0) / len(translations)
    outs.write("\n\n ------------------------------------------- \n\n")
    outs.write(str(mean))