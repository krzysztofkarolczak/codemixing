#!/usr/local/bin/python
# encoding: utf-8

# author: Krzysztof Karolczak
# date: 13.09.2013

"""
User is presented with a set o 20 Y/N questions to determine initial level of language proficiency,
base on the score the user is assigned to one of five levels. 

Based on this selection an appropriate text is chosen. The text is preliminary prepared by 
dicionary_prepare is presented to the user with chosen nouns replaced with English words.

After the user confirms he/she has read the text a set of max. 20 questions is shown. 
The objective for each question is to type the word that is given in Japanese and 
also partly in English (random letters are replaced by "_"). In case o error 
the software tries to find a example sentence with a proper use of the word
and show it to the user (sentences come from Tanaca corpus).

If user has made any mistakes at the end a 2nd round of questions is presented, this time more 
difficult as there is no information about the length and no letters are provided. However this time
an English sentence with a blank and a Japanese translation is given.

All the actions of the user are recorded in a result file with the appropriate date and time.  
"""

import os
import logging
import random
from datetime import datetime
import podobne
from random import randint
from random import choice

ENGLISH_MODE = 1 ## Change to 0 to have code-mixing !

FREQ_LIST_PATH = "res/freq_list.csv"
OUT_FILE_PATH = "res/texts_output/" 
CORPUS_PATH = "res/corpus/tanaka_corpus.txt"
FILE_EXTENSION = ".txt"
EDICT_BYDIFF_PATH = "res/diff/edict_diff_"
    
logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.ERROR)
logger=logging.getLogger(__name__)

if(ENGLISH_MODE == 1):
    ENGLISH_PREFIX = "eng"
else:
    ENGLISH_PREFIX = ""

corpus_sentences = []

## If the english mode is on - we add a "eng" suffix to our result file name. 
RESULT_FILE_PATH = "results/" + ENGLISH_PREFIX + datetime.now().strftime("%Y%m%d %H%M%S") + ".txt"
SEPARATOR = "\n\n ------------------------------------------- \n\n"

## Store all results in a file with a name given by the current date and time
result_fhandle = open(RESULT_FILE_PATH, 'a')
result_fhandle.write("Test start: " + datetime.now().strftime("%Y.%m.%d %H:%M:%S") + "\n");

def initial_questions():
    """
    20 initial questions are prepared based on frequency of occurrence list.
    Chosen words have an increasing difficulty and there are 5 questions
    for each 1000 words in list. 
    """
    
    with open(FREQ_LIST_PATH, 'r') as content_file:
        content = content_file.read()
        
    diff = content.split("\n")
    
    ## We randomize the starting words and continue by doing 200 word jumps

    word_index = randint(0, 200)
    i = 1
    score = 0
    
    ## We have 4000 words in our list
    while(word_index < 4000):
        word = diff[word_index].split(";")
        q_string = "[Q " + str(i) + "/20] Do you know the word '" + word[0] + "' [Y/n]: "
        usr_input = raw_input(q_string)
        
        ## Continue asking the same question until user enters y or n (case insensitive) 
        ## or blank (counts as yes)
        while(usr_input not in ['Y', 'y', 'N', 'n', '', ' ']):
            usr_input = raw_input(q_string)
        
        if(usr_input in ['Y', 'y', '', ' ']):
            ## If the answer is correct add to the score the difficulty of the word.
            ## The way we calculate it is: divided the frequency of occurrence by 1000
            ## and round to integer (so for 8k words we get a number from 0 to 8) and 
            ## add 1 to eliminate the 0
            score += int(int(word[1])/1000)+1
            result_fhandle.write(word[0] + "|y|" + str(word_index) + "\n")
        else:
            result_fhandle.write(word[0] + "|n|" + str(word_index) + "\n")
        
        word_index += 200 
        i += 1
    
    ## Divide by 10 to get a score from 0 to 5.
    final_score = (score+0.0)/10
    result_fhandle.write("-- Final score: " + str(final_score) + " --\n")
    result_fhandle.write(SEPARATOR)
    
    print "\nYour language level is %0.1f / 5" % final_score
    
    return int(final_score)

def show_text(score):
    """
    Presents the text based on achieved score. Also prepares the content of all questions.
    """

    ## We don't have a text for the highest level of difficulty 
    if(score == 5):
        score = 4
    
    ## Case where there is no positive answers in the initial test
    if(score == 0):
        score = 1
        
    ## Change the score from 1-4 to 5-2 to corresponds to Japanese proficiency levels
    score = 6 - score
    
    ## Read the source file and split over markers
    with open(OUT_FILE_PATH + str(score) + FILE_EXTENSION, 'r') as content_file:
        content = content_file.read()
    content_split = content.split(SEPARATOR);
    
    ## First part of content is the whole text prepared for code-mixing [see: dictionary_prepare.py]
    text = content_split[0]
    ## Second part is the list of words to be replaced. 
    source_words = content_split[1].split("\n")
    
    ## If the English mode is on - just print the full text in English. 
    if(ENGLISH_MODE == 1):
        with open(OUT_FILE_PATH + ENGLISH_PREFIX + str(score) + FILE_EXTENSION, 'r') as content_eng_file:
            text = content_eng_file.read()
    ## If the English mode is off - replace the selected jp words by eng.
    else:
        for word in source_words:
            if(len(word) > 0):
                trans = word.split(" | ")
                jp_word = trans[0].strip()
                en_word = trans[1].strip()    
                text = text.replace(jp_word, en_word)
        
    print "\nPlease read the whole text \n\n" + text + "\n\n"
    raw_input("Press any key to continue")
    
    ## Wait for key press and clear the screen (works only on linux console)
    os.system('clear')
    
    return source_words

def end_questions(source_words):
    """
    Create and display the end questions and count the score
    """
    score = 0
    incorrect_words = []
    correct_ans = { }
    incorrect_ans = { }
    
    ## Prepare the corpus file
    ins = open(CORPUS_PATH, "r" )
    
    for line in ins:
        ## Only "A: " lines have the sentences 
        if(line.startswith("A:")):
            ## We can omit the first 3 chars and everything that is after the #
            prepared_line = line[3:].split("#")[0]
            corpus_sentences.append(prepared_line)
    
    ## Shuffle the list of words
    random.shuffle(source_words)
    
    ## Leave only 20
    if(len(source_words) > 20):
        del source_words[20:]
    
    j = 1
    q_len = str(len(source_words))
    
    ## For each word that is left create a question
    for word in source_words:
        if(len(word) > 0):    
            ## The line format is: ([index])[kanji] | [translation] | [difficulty]
            trans = word.split(" | ")
            jp_word = trans[0].strip().split(")", 1)[1]
            en_word = trans[1].strip()
            diff = int(trans[2].strip())
            
            question = ["[Q " + str(j) + "/" + q_len + "] " + question_generator (jp_word, en_word), diff]
            result_string = en_word + "|" + jp_word + "|" + str(diff) + "|"
            j += 1

            inpt = raw_input(question[0])
            if(inpt.strip().lower() == en_word):
                print "Correct!"
                score += 1
                
                ## The index of the dictionary are the difficulty levels (1 to 8) so we count
                ## the number of correct answers for each difficulty level
                if diff in correct_ans:
                    correct_ans[diff] = correct_ans[diff] + 1
                else:
                    correct_ans[diff] = 1
                    
                result_fhandle.write(result_string + "y\n")
            else:
                print "Incorrect. It should be: '" + en_word + "'"
                incorrect_words.append([jp_word, en_word, diff])
                
                ## As previously but for counting incorrect answers
                if diff in incorrect_ans:
                    incorrect_ans[diff] = incorrect_ans[diff] + 1
                else:
                    incorrect_ans[diff] = 1
                    
                ## We try to find a sentence with an example use of the word
                ## end display it if it is found
                s = find_similar_sentence(jp_word, en_word)
                if(s):
                    print s
                     
                result_fhandle.write(result_string + "n" + "|" + inpt.strip().lower() + "\n")

    result_fhandle.write(SEPARATOR)
    result_fhandle.write(" -- Summary -- \n")
    
    no_of_questions = len(source_words)
    
    ## Summary statistic of the form: [difficulty_level<1:8>]:[no. correct ans]|[no. incorrect ans]|[total] 
    for diff_level in range(1,8):
        correct = 0 
        incorrect = 0
        
        if(diff_level in correct_ans):
            correct = correct_ans[diff_level]
 
        if(diff_level in incorrect_ans):
            incorrect = incorrect_ans[diff_level]
  
        result_fhandle.write(str(diff_level) + ":" + str(correct) + "|" + str(incorrect) + "|" +  str(correct+incorrect) + "\n")
        
    ## Score is simply calculated as a percentage of correct answers
    percent_score = ((score * 100) / no_of_questions)
    result_fhandle.write("-- Score --\n")
    result_fhandle.write(str(score) + "|" + str(no_of_questions) + "|" + str(percent_score) + "%\n")    
    result_fhandle.write(SEPARATOR)
    
    print "\nYou got %d / %d (%d %%)" % (score, no_of_questions, percent_score)
    
    return incorrect_words

def end_questions_2nd_round(incorrect_words):
    
    ## Wait for a keypress and clear screen. [Linux specific]
    raw_input("Press any key to continue")
    print "\nPlease fill the missing word \n\n"
    os.system('clear')
    
    incorrect_words_2nd_round = []

    score = 0
    iq_len = 0
    
    ## Iterate over all words incorrectly inputed in the 1st round of questions
    if(incorrect_words and len(incorrect_words) > 0):
        for word in incorrect_words:
            sentence = find_similar_sentence(word[0], word[1])
            ## We create a question only when an exemplary sentence exists
            if(sentence and len(sentence) > 0):
                ## Replace the eng words by jp word in brackets and followed by a blank 
                sentence = sentence.replace(word[1], "(" + word[0] + ")________")
                q_out = sentence + ":\n"
                ans = raw_input(q_out)
                iq_len += 1
                
                result_string = word[1] + "|" + word[0] + "|"
                
                if(ans.lower().strip() == word[1]):
                    print "Correct!"
                    score += 1
                    result_fhandle.write(result_string + "y\n")
                else:
                    print "Incorrect. It should be: '" + word[1] + "'"
                    result_fhandle.write(result_string + "n" + "|" + ans.strip().lower() + "\n")
                    incorrect_words_2nd_round.append(word)
    
        result_fhandle.write("-- Score 2nd round --\n")
        percent_score = ((score * 100) / iq_len) 
        
        result_fhandle.write(str(score) + "|" + str(iq_len) + "|" + str(percent_score) + "%\n")
        result_fhandle.write(SEPARATOR)
        print "\nYou got %d / %d (%d %%)" % (score, iq_len, percent_score)
        
    return incorrect_words_2nd_round

def get_random_word(diff):
    """
    Get a random line from file. Not very optimal, but hey it works. 
    """
    
    file_path = EDICT_BYDIFF_PATH + str(diff) + ".utf"
    return random.choice(list(open(file_path)))

def get_words_for_anwers(word, diff):
    """
    Get the initial word + 2 unique words from the prapared dictionary lists (based on difficulty)
    + 4th one trying to find similar phonetical to one of the previous three. If this fails just
    grab another one from the list. Shuffle the words before returning.
    """
    
    words_for_answers = []
    
    words_for_answers.append(word)
    
    while(len(words_for_answers) < 3):
        new_word = get_random_word(diff)
        if(new_word not in words_for_answers):
            words_for_answers.append(new_word)

    random.shuffle(words_for_answers)
    
    for answer_word in words_for_answers:
        podobne_word = podobne.podobne(answer_word)
        if(podobne_word and len(podobne_word) > 0):
            words_for_answers.append(podobne_word)
            break
        
    while(len(words_for_answers) < 4):
        ## If we are here we didn't find a similar sounding word
        new_word = get_random_word(diff)
        if(new_word not in words_for_answers):
            words_for_answers.append(new_word)
    
    random.shuffle(words_for_answers)
    
    return words_for_answers

def end_questions_3rd_round(incorrect_words):
    """
    Prepare and display the 3rd round of questions.
    """
    
    score = 0
    iq_len = 0
    words_for_answers = []
    
    random.shuffle(incorrect_words)
    
    if(incorrect_words and len(incorrect_words) > 0):
        for word in incorrect_words:
            sentence = find_similar_sentence(word[0], word[1])
            if(sentence and len(sentence) > 0):
                sentence = sentence.replace(word[1], "________")
                q_out = sentence + ":\n"
                q_out += "\nType in the right word:"
                print(q_out)
                
                words_for_answers = get_words_for_anwers(word[1], word[2])                 
                
                q_out = words_for_answers[0].decode('utf-8').strip() + ", "
                q_out += words_for_answers[1].decode('utf-8').strip() + ", "
                q_out += words_for_answers[2].decode('utf-8').strip() + ", "
                q_out += words_for_answers[3].decode('utf-8').strip() + "\n"
                ans = raw_input(q_out)
                iq_len += 1
                
                result_string = word[1] + "|" + word[0] + "|"
                
                if(ans.lower().strip() == word[1]):
                    print "Correct!"
                    score += 1
                    result_fhandle.write(result_string + "y\n")
                else:
                    print "Incorrect. It should be: '" + word[1] + "'"
                    result_fhandle.write(result_string + "n" + "|" + ans.strip().lower() + "\n")

    result_fhandle.write("-- Score 3rd round --\n")
    
    percent_score = ((score * 100) / iq_len) 

    result_fhandle.write(str(score) + "|" + str(iq_len) + "|" + str(percent_score) + "%\n")
    result_fhandle.write(SEPARATOR)
    print "\nYou got %d / %d (%d %%)" % (score, iq_len, percent_score)

"""
def swap_letters(word):
    rint2 = rint1 = randint(0, len(word)-1)
    while(rint2 == rint1):
        rint2 = randint(1, len(word)-2)
        
    word_l = list(word)
        
    word_l[rint1], word_l[rint2] = word_l[rint2], word_l[rint1]
    return "".join(word_l)
"""

def find_similar_sentence(jp_word, en_word):
    """
    Find all sentences that have the English word and its Japanese version in the translation.
    Return a randomly chosen one or null if not found.
    """
    matches = [x for x in corpus_sentences if (jp_word in x and en_word in x)]
    if(len(matches) > 0):
        return "Example sentence: \n " + choice(matches) + ""

def question_generator(jp_word, en_word):
    """
    Generates end questions based on replaced nouns. The user is asked to input a word, 
    he/she is given with the Japanese word and an English hint. 
    """
    question_str = "Please type the english word for " + jp_word + " ("
    
    len_en_word = len(en_word)
    
    for i, l in enumerate(list(en_word)):
        
        ## if the word is less than 5 letters there is 5% of unveiling a letter, 
        ## 15% when length is <5, 10), 25% with length <10, 15) and 35% for 15+
        if(len_en_word < 5):
            p = 5
        elif (len_en_word < 10):
            p = 15
        elif (len_en_word < 15):
            p = 25
        else:
            p = 35
            
        ## We always show the last and first letter and characters like spaces, dots and colons
        if(l == ' ' or l == '.' or l == ',' or randint(1, 100) < p or i == 0 or i == len(en_word)-1):
            question_str += l
        else:
            question_str += "_"
            
    question_str += "):"
    return question_str

## Ask the initial questions and calculate the score
score = initial_questions()
## Select and display the text, return the dictionary of words for testing
words_for_testing = show_text(score)
## Select max. 20 words, ask the questions. Returns the dict. of incorrect answers.
incorrect_words = end_questions(words_for_testing)
## Perform the 2nd round of testing only for the words that were not ans. correctly.
incorrect_words_2nd_round = end_questions_2nd_round(incorrect_words)
## Perform the 3rd round of testing only for the words that were not ans. correctly in 2nd round.
end_questions_3rd_round(incorrect_words_2nd_round)

print "Thank you for taking the test. Your precious time has contributed to making learning English easier!"

result_fhandle.write("Test end: " + datetime.now().strftime("%Y.%m.%d %H:%M:%S") + "\n");
result_fhandle.close()

## Mazzilla
##                                 .ammmmmmmmma.
##                             .:ahhmmmmmmmmmmmmma.
##                           .ihhhmhmmmhhhhmmmmmmmma.
##                         :ihihhmhmymhhyyhmmmmmmmmmma.
##                       .iyyihmhhmmmmmhhyyyhhmmmmmmmmma.
##                      .iyiiyhmmmmmmmyhhyyyhhmmmmmmmmmmma.
##                    .yyiiyyhhhmmmmmmyhhyhyhmmmmmmmmmmmmma.
##                   .yyiiyyyhhhmmmmmhyyhhyyyhmmmmmmmmmmmmma.
##                   .yyiiyhyhyhhmmmmmyyyhhyyhmmmmmmmmmmmmmmma
##                  .yyiiiyhhyhhhhmmmhyhyhhhyhmmmmmmmmmmmmmmmma
##                 .iyyiiiyyhhyhhhhhmhyhyyhyhhmmmmmmmmmmmmmmmmm.
##                 yyyiiiyyhhmmhyyiyhyyiyyyhyhmmmmmmmmmmmmmmmmma
##                .yyyiiyhhmmhyi:.. .::iiyhyhmmmmmmmmmmmmmmmmmmma
##                yyyyihhmmhi:..        . .:ymmmmmmmmmmmmmmmmmmmma
##               .yyyihhhmmhi::.         . .:ihmmmmmmmmmmmmmmmmmmm.
##               yyyihmhmmmh::.            ..:ihmmmmmmmmmmmmmmmmmma
##              .yyyihmhhmmi:               .:.ihmmmmmmmmmmmmmmmmmma
##              hyyiyhhmhmm:,                .:.:vmhmhhmmmmmmmmmmmmm.
##              hyiyyhhhmmh::hii::.         .ahmmi:vmhhmhmmmmmmmmmmma
##             .hyyyhmhmmmi: .:ama,'      .ii,...::ivmhhmhmmmmmmmmmmm.
##             iyiyyhhmhmm:::, vwv,.    .: .:imai:..:immhyhhmmmmmmmmma
##             yiyyhymmmmi:.::....:,  . .::, vwv .,..:mmmmhyhhmmmmmmmma
##             iyyihmhmmm::. .  . .,   .:.:.:.::,.:::.immmhyyhhmmmmmmmm.
##             :yiyhyhmmm:. .. .       .:.::..    .:..mmmhyyhyhhmmmmmmma
##            .iyyyhmhmmma. .          ::.:..    .::.:mmmhyhyhyhhhmmmmmm
##            .yyiyyhmmmma..    ' .    ..:.:.:   ..::ammmhmmhmmhyhymmmmm.
##            :iyiyyhhmmmm:.     .     .:.::.:. .:.:immmmhmmyyhmhhyhymmma
##            .yyyiyhhmmmm:..        .'::::.:. ...:immmmhhmyhmhmhyymyhmmm
##            .hyiyyhhmhmma:.        .  .::.:. ..::ammmhhhyyhhymhyhymymmm
##            :hyiiyhhmmhmma:.  .,,:;;:;;;,.. .::.:mmmmhmmhymhhmhhhmmhymm
##            ihhyiyyhhmmmmma..   ,:;;;;;:,. .:.:ammmmmhmhhmhmmmmmhmmhymm
##            :hhhyhhhmhmmmmma.     ,,',, . .::amhmhhmmyymhhmmmmmhmhhhamv
##            ihyhyyhhhhmmhmmmma      . . .::iahhmmhhmmhyyhmmmmmmmhhhhv,
##            :hhhyyhhhmhmhmmmmma.  . . ..::iahmmhmmmmhyyhmmmmmmmhhhhv    ...
##            ,yhiyyhhhmhmhhmmmmmh:......:::iihhyymhhyyhmhmmmhyymmmmmmahmahmha.
##             :hyiyhhhhmhmmhmmmmmhi':::.   ..vhyyhyhyiyyhmmmhmmmmmmmmmmmmmmmmm
##              ,iyiiyhhhmhmmhmmmmmh,;::..   .ayhhhhyyyhyhhyhi:::iimmmmmmmmmmmm
##               ,yiiiyhhhmhmhmmmvmv ;::. .  ahhhyhhyhhhyyv,  .:i::mmhmmmmmmmv,
##               ,:iiiihhhmmhmhmmvai :::. . .iyhhyhhhyyhhy.   . .:.:mmmmmm;;v,
##                :iiiyhhhmhmhmmm.mi.;::.: :iiyhyhhhyyhhyy,  , ..:.:imhmmv:.:.
##                :iiiyhhhmhmhmmm.mi.;::.: :iiyhyhhhyyhhyy,  , ..:.:imhmmv:.:.
##                  ::iyhhhmhmaiiii;;::. .i.iiyhyhhyyhmyii,     .:hhyhmhv:...::
##                   i:ihmhhmhmhiiii;;:, :hi.yhhyhhyyhmi:,     .hhyyhy..:..:..
##                   ii::yhhmmhmmii;:;:  .h.iyhyhhhyyhm.:     :hyh:hh:...:..:.
##                   .i:iyhhymmhi;:;i;:   .i:ihyhhhyyhm.,.     ,:,.ii:.:..   ..
##             .      :i:::ihhmmhii;i:.  i .:iiyiyhhyyyhmyi:
##          .:i      .:i::ihhhmviii;:    :.  ,hhymhyyiyhmhh:.
##        .::,i     .:iii:ihhhviii;.     ,:;iihyyhyhhyyhhmh:..
##       ..:hih.  .:ihhy:iyhmviiii;,          ,hyhmyhhhyyhyh::.
##       .:hihiyyyhhhyyiyyhyhmyiii,            iyhhyhhhyhyi:::..
##        .:ihihyhhyhyyiyyiyhhvii,             :yhmhmhmhyyy:..::.
##                   yhhyyyhvvi'               iyhhyhmhhyyv..:.::..
##            ,viyhhhyhhyiyyhii:                ahmhmhmyhyh:..:..;.:..
##              vhyhhhyhhmvi::,               ahmmhmhyhhy..:.:..:ii::..
##               ,vhyyhyv::;:          -.:..ahmhhyyhhh::.:..:.::..:ia:.
##                .  ,:,, ::,         :. 'iyhyhmhhyyv:...::.::.::..:.ia:.
##              .         ,          ,,:ihhhhi:iyy::;:.:::..::;:.:.. .:a:
##            .,                        ''..:::..  . . ..::.:.:..:...:.:ai
##           .             . .                       .:..:.::.::...:..:.:ia.
##          :.             ..                          :.:::.:::.:... ..:iima
##         .:.             ,                         . ..:::.::..:.. ...:immh
##        o.:.            ,                         . : ..::.::... .. ...::imv
##         .,:           ,     .::.                 .:.:::;;.:.. .... ..::.iv
##          ,.          .:    .:;::.               ..: :::;;:.:.. ...::::.iv
##           ,.      . .::   .:;:::.             . .:.:.:;;:.. .. . .:.:::v,
##            ,.  ..  .:.:   ..():::             .....:::;;:;.:. .. ..:::i,
##             ,. . ..:.::.  ::::.:            . .....:::;;;..  . ..:.::::,
##              ,:.:..::::.   ,::,             ...::.::.:;;..   .....:..::
##                ,::::::...                 . ...:.:::::;;;.   ....:.::::
##                     ,,....              .. ..:.:.:;:;;:..  . ....:.:::,
##                      ,.....         .  ...:....:.iii::..   .:.:.:::iii
##                      . .... .  . ..  .. ...:.:;iiii::.   ..:.:.:;:;ii,
##                      . ..... ,. . .. . ...:.;iii;i;.    ..::...:;i;ii
##                      . .:..     ,::::.::ii;i:...       ..:... .:;;ii
##                      ,..:.         ,,,::::::,,        ..::..: ;;;;i,
##                       ..:..                        ..:.::...:;;i;ii
##                       ,.: . .                  .  .:.. ...:;:;i;ii,
##                        .. . ..                 ..:.... .::;;:;i;ii
##                       .a ..  .              .:.::. :..::;:i;::;i,
##       ..--::. .ammmmmmmma..                   .: :..:.:::;;;.:;ii
##    ..   ..   ...vmmmmmmmm. .                      .::::::;:.:ii;i
##         ,:.    .::vmmmmmma.                     .:.::.:::..:;iih,
##    ..     ,:. . .::.,vmmmm. .,               . ...:..:  .::;;iih
##    mmmma.     ..  .::..vmmi.     .           ... ... .. ::;iihih
##    mmmmmmma.    .  .::..:'.:. .   .. .     . ..:.:. ..:::;:;iiiia
##    mmmmmmmma.   ..  :, .  & :,    .. ..:. ...:....:...:::;;;;iiim.
##    mmmmmmmmmm.  ..   ..  ..... ,   .... .:. ..  ..:..::;::;;::;ima.
##    mmmmmv',    ....  ..  ...:.,,. .. ..   .. . .:..:..:.;:;;;:;iihma.
##    mv':..  .  .......  .:: .:,. : ....   .  ...:. :....:;:;:;:;iihmmma.
##    .::....  .......... .::ii:.: : ... .    ... . . .:.::;:;;.;::iihmmmma.
##               ,,,,,,,        .:,.  ..  .      .. . ..:..:;;:;:::;;iimmmmma.
##                              .: . ..  .    . .. . ..:.::..;;::.:;;iihmmmmma
##                              y.  .. .    .   . .  .. .. ..:..::;;iihmmmmmmm
##                              y/. . ...   ..    ..  . .:.:...:;;;:;iimmmmmmm
##                               y/. .. .        . .. . .....;..::;;;iihmmmmmm
##                               y//..  ..   .      .  .  .:.: ..:;:;;ammmxxmm
##                               y/y//.. .    .       .. . . :..::;:yyxmxxxmmm
##                              ///yy//yy...  ..        . . ::;:.yyyxmyxxmmmmv
##                            ////yy////y///...   ..    . ...:yyyyyy//mmxxxm.
##                           .////y//yy//////y///////......//yyyyyx//yxxxxxxm.
##                          /y////y///yy///////////y///yyyyyyyyy/yyxyyyyxxxmv
##                         ///y////y///yx//y/////////////yy///yy::::::yyxv. .
##                       //////y///y////yx/xy/y////y//yyyxxv, .. ..,::::.  ....
##                     //////y/y///y///xyyxxyyy///yyyxxv,,   . ...:::.::.  ..
##                    //////yy//yy//yy/xyy//xxyyxxyyv',    ..   ..:..::....  ..
##                  ///y/////yy//yy//y/xyy//xxyyyyyv     .::   ..::::.:....:...
##                ////y/yy//y///y//yy/yyy///yyxyy/v   ...ai,  .:.,..::.... ...
##              ./////yxy//yyy///yyyxy/y/y///yyyyyy .:.avx   .:. ...:::..  ...
##            ./////yyyyxxyyyyyy///yyyyyxyx////yyyy  .axy .  .:..,..::.  .  ...
##          .///y///yyyyyyyxxyyyy///yyyyy/yxy////yy .:xy,   i,  .. . .. .mmc
##         ///y/yxyyyyyyyyyxxxyxyyyyyyyyyy//y//////:.axx   .ai   i   ..:.::.:::
##       ////y//yxxyyyyyyyyyyyyxyxyxxyyyxxxy///y/////yyx .;i-;. ai   ..::::xxxx
##     ////////yy//yyyyyyyyyyyyyyyxyxyxxyxxxy////y//////a.;hi v'h:  .:xxxxxxxxx
##     //y///y//yy//yxxyyyyxyyyyyyyyxxyyxyxmxyy/yy//yy///y:.ai  .m: .axxxxxxxxx
##     ///y///yy//yyyxxyyyxxyyyxyyyxxyyxxxmxm/y///yy///yy/ym, .axa.axxxxxxxmmxx
##     //y//yyy/yyyxyyyyyxyxxxyyyyyxxxxxxxxmm//y///yy/////yy:.axxyyyxxxxxxmmmxx
##     //yy//yy/yyyyyxyyyyyyxyxxyyxxyyxxxxymmy//y///yyy/yyyyyyyxyxxxxmmmxxxxxxx
##     /yy//yyxyyyyyyyyyyyyyyyyxxxyxxxxmxxmmmm///y//y//yyyyy//yxyyxxxyxxxxxxmxx
##     yyyyyyyyyxxxyyxyyxxxyxxxxxxxxxxxxxmmxmm////yy///yyyyxxxxxyyxxxxxmxxxxmxx
##     yyyyyyxyxyyyyxxxyyyxxxxxxxxxxxmxxxmxmmm///////yyyy//yyxxxmmxxxxxxmxxyyxx
##     yyyyyyyxyxxxyxyyyyxxxxxxxxxxmxxxmmxmmv,/////yy///y//yyyyyxxyxyyyyyxxxyyx
##     yyyyyyyxxyyyyyyxxyxxxxxxxxmxxxmmxxmv,  ///////yyyxxyyyyyyyxxxxxxxxyyyyyx
##     yyyyyyyyyyyxxyyxyxxxxxxxxxxmxxmmxv,   ///////yy//yy//yyyyyyyyyyyyyyyxxxx
##     yyxyyxxxxyxyyyxxxxxxxxxxxmmxmmxm,     ////////yyyyxxxyxxyyxxxxxxxxxxyyxx
##     yyyyyyyyyyxxyyxxxyxxxxxxxmmxmv,       /////y//yyyyxyyyyyyxxyxxxyyyyxxyxx
##     yxxxyxxxyxxxxxxxxxxxmmxmmxmv,        ////y//yyyyyyyyyyyyyyxyxxxxxxxyyxxx
##     yyyyyyyxxyxxyxxxxmmxxmmxxv,          /////yy/yyyyyyyyyyyyxxyyyxxyxxxxxxx
##     yyyyyxxxyxxxyxxym                    ///yy//yyyyyyyyxyyyxxyyxxyxxxxxxxxx
##     xxyyxyyxxyyyyyymyymmxv,             .///yy/yyyyyyyyyyxyyyxyyyyxxxxxxxxxx
##     yyxyyyyxxyyymymxxmmma.              /////yy/yyyyyyyyyyyyyyyyyxxxxxxxxxxx
##     yyyyyxxyxyyyxxymmmmmmmma.           /////y/yyyyyyyyyyyyyyyyxyyxxxxxxxxxx
##     yxxyyyyxxyxmmmxxmmmmmmmxxxa         //////yy/yyyyyyyyyyyyyyyyyxxyxxxxxxx
##     yyyxxxxmmmxxmxmmmmxxxymmmyyy.      .////yy/yyyyyyyyxyyxxyyyxxxxxxxxxxxxx
##     xmmmxxxyxxxxmmxxxxxxxyxxmmxxyy.    ///y/yyyyyyyyyyyyxyxxxxxxxxxxxxxxxxxx
##     yyyyxxxxxxxyyyxxxyxxxxxxxxmmxxyy.  //yy/yyyyyyyyyyyxyyyyxxxxxxxxxxxxxxxx
##     yxxxxxyxyyyxxyyyxxyyxxxxxxxyyxxxmmx//y//yyyyyyyyyyyyxyyxxyxyxxxxxxxxxxxx
