#!/usr/bin/env python
import sys
import logging
import codecs
import re

if __name__ == "__main__":
	#logging.basicConfig(level=logging.DEBUG)
	logging.basicConfig(level=logging.ERROR)
	# xyz
logger=logging.getLogger(__name__)

def podobne(slowo):
	#logger.debug("Szukamy %s", slowo)
	mydic = codecs.open("res/same_readings.txt", "r+", "UTF-8")
	czytania = mydic.readlines()
	for czytanie in czytania:
		czytanie = czytanie.rstrip()
		slowa = czytanie.split(':')[1].split("-")
		slowa = [ x.strip("[]") for x in slowa ]
		#logger.debug("%s", slowa)
		
		if slowo in slowa:
			slowa.remove(slowo)
			#logger.debug("Znalazlem %s", slowa[-1])
			
			return slowa[-1]
		
	return ""

if __name__ == "__main__":
	print podobne(sys.argv[1])
	
