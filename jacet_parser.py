#!/usr/local/bin/python
# encoding: utf-8

# author: Krzysztof Karolczak
# date: 09.08.2013

"""
Combines the jacet list consisting of separate .txt files into a single file of the format
[word];[index]
"""

import re
import logging
import codecs

JACET_PATH = "jacet/" 
FREQ_LIST_PATH = "res/freq_list.csv"

logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.ERROR)
logger=logging.getLogger(__name__)

## Open file resources
count = 1
freq_list = codecs.open(FREQ_LIST_PATH, "w", "utf-8")

for i in range(1,9):
    path_string = JACET_PATH + "jacet" + str(i) + "000.txt"
    print path_string
    with open(path_string, 'r') as content_file:
        content = content_file.readlines()
        
    for word in content:
        if(len(word) > 1):
            freq_list.write(word.rstrip('\r\n') + ";" + str(count) + "\n")
            count += 1
            
freq_list.close()